# start by pulling the python image
FROM python:3.10-alpine

# copy the requirements file into the image
COPY ./req.txt /app/req.txt

# copy every content from the local file to the image
COPY ./app /app

# switch working directory
WORKDIR /app

# install the dependencies and packages in the requirements file
RUN sed -i 's/https/http/' /etc/apk/repositories
RUN apk update && apk add tzdata
ENV TZ=America/Sao_Paulo
RUN apk add --no-cache bash
RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev libffi-dev
RUN pip install psycopg2-binary
RUN pip install -r req.txt
RUN sed -i 's/^SQLALCHEMY_DATABASE_URI.*$/SQLALCHEMY_DATABASE_URI = "postgresql:\/\/postgres:123mudar@db:5432\/proj_flask"/g' /app/config.py
RUN sed -i 's/^ROOT_PATH = ""/ROOT_PATH = "\/proj_flask"/' /app/config.py

ENTRYPOINT ["./script.sh"]
