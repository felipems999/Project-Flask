import os


SECRET_KEY = os.environ.get("SECRET_KEY") or "vcnuncasabera"

APP_NAME = "Project_Flask"

SALT = "hash"

# Configurações de banco de dados
SQLALCHEMY_DATABASE_URI = "postgresql://postgres:123mudar@localhost:15432/proj_flask"
SQLALCHEMY_DATABASE_URI_TEST = "sqlite:///:memory:"

# Pasta raiz do projeto
ROOT_PATH = ""

# Retorna o caminho absoluto do diretório do arquivo atual
BASEDIR = os.path.abspath(os.path.dirname(__file__))

# Habilita o modo debug
DEBUG = True

# Diretórios de upload
UPLOAD_FOLDER = os.path.join(BASEDIR, "app/static/uploads")
UPLOAD_FOLDER_PHOTO = os.path.join(BASEDIR, "app/static/uploads/images/photo")

# Extensões permitidas para upload
ALLOWED_EXTENSIONS = set(
    [
        "doc",
        "docx",
        "gif",
        "jpeg",
        "jpg",
        "json",
        "odf",
        "odt",
        "pdf",
        "png",
        "txt",
        "xls",
        "xlsx",
        "xml",
    ]
)

# Extensões permitidas para upload de application/
ALLOWED_EXTENSIONS_APPLICATION = set(
    [
        "json",
        "pdf",
        "xml",
    ]
)

# Mime types permitidos para upload
ALLOWED_MIMETYPES = set(
    [
        "application/*",
        "image/*",
        "video/*",
    ]
)
