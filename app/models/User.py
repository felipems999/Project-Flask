from . import db
from flask_login import AnonymousUserMixin, UserMixin
from sqlalchemy import func
from werkzeug.security import generate_password_hash as gph
from config import SALT


class User(db.Model, UserMixin):
    __tablename__ = "users"

    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    name = db.Column(db.String(50), nullable=True, unique=False, default="")
    surname = db.Column(db.String(50), nullable=True, unique=False, default="")
    username = db.Column(db.String(50), nullable=False, unique=True)
    email = db.Column(db.String(50), nullable=True, unique=True)
    password = db.Column(db.String(130), nullable=False, unique=False)
    img_profile = db.Column(db.String(256), nullable=True, default="")

    @property
    def serialize(self):
        return {"username": self.username, "email": self.email}

    @staticmethod
    def create(username, password, email="", name="", surname=""):
        new_user = User(
            username=username,
            email=email,
            password=gph(password + SALT),
            name=name,
            surname=surname,
        )

        db.session.add(new_user)
        db.session.commit()

    @staticmethod
    def get_users():
        return [
            {
                "id": user.id,
                "username": user.username,
                "name": (user.name + " " + user.surname),
                "email": user.email,
            }
            for user in User.query.order_by("id").all()
        ]
