from flask import Flask
from flask_migrate import Migrate
from flask_bootstrap import Bootstrap5
from flask_wtf import CSRFProtect
from config import ROOT_PATH, DEBUG
from models import db
from routes import register_blueprints

app = Flask(__name__, static_url_path=ROOT_PATH + "/static")
app.config.from_object("config")
db.init_app(app)
bootstap = Bootstrap5(app)
csrf = CSRFProtect(app)
migrate = Migrate(app, db)

register_blueprints(app)

if __name__ == "__main__":
    app.debug = DEBUG
    app.run(host="0.0.0.0")
