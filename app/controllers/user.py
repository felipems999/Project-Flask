from app import app
from flask import render_template
from app.forms import LoginForm


def login():
    form = LoginForm()

    return render_template("login.html", title="Logar", form=form)
