from flask import render_template


def index():
    user = {"username": "Melissa"}
    posts = [
        {
            "author": "Felipe",
            "text": "Aprendendo Python Flask, um microframework para criar webapp",
            "date": "22/01/2023",
        },
        {
            "author": "Melissa",
            "text": "Visitando hospital na aula de hoje!",
            "date": "20/01/2023",
        },
        {"author": "Flávio", "text": "Primeiro dia de aula", "date": "1 ano atrás"},
    ]

    return render_template("index.html", title="home page", user=user, posts=posts)


def about():
    description = None

    return render_template("about.html", description=description)
