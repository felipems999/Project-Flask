from flask import Blueprint

from controllers.user import login

user_rt = Blueprint("user", __name__)

user_rt.route("/login", methods=["GET", "POST"])(login)
