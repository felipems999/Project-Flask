from flask import g
from app.config import ROOT_PATH, ALLOWED_EXTENSIONS

from routes.home_rt import home_rt
from routes.user_rt import user_rt

modules = {
    ROOT_PATH + "/": home_rt,
    ROOT_PATH + "/login": user_rt,
}


def load_config():
    g.valid_file_extensions = ALLOWED_EXTENSIONS
    g.root_path = ROOT_PATH


def register_blueprints(app):
    for url, module in modules.items():
        app.register_blueprint(module, url_prefix=url)
