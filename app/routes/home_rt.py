from crypt import methods
from flask import Blueprint

from controllers.home import index
from controllers.home import about


home_rt = Blueprint("home", __name__)

home_rt.route("/", methods=["GET"])(index)
home_rt.route("/about", methods=["GET"])(about)
